
layout["Code"] = {
  Style = "Text", TextBoxStyle = "Normal", 
  Position = {0,0}, Size = {0,0}
}

table.insert(graphics,{
  Type = "Label", Text = "Status Name",
  HTextAlign = "Left",
  VTextAlign = "Center",
  Position = {4,4}, Size = {72,16},
})

layout["named.status.component"] = {
  PrettyName = "Status Name",
  Style = "ComboBox",
  TextBoxStyle = "Normal",
  Position = {76,4},
  HTextAlign = "Center",
  Size = {120,16},
}

table.insert(graphics,{
  Type = "GroupBox",
  Text = "Status",
  StrokeWidth = 1,
  Position = {4,24},
  Size = {335, 150},
  CornerRadius = 8,
  HTextAlign = "Left",
})

table.insert(graphics,{
  Type = "Label", Text = "Indicators",
  HTextAlign = "Left",
  VTextAlign = "Center",
  Position = {8,48}, Size = {72,56},
})

layout["bridge.video.peripheral.usb.speed.icon"] = {
  PrettyName = "USB Connection",
  Style = "Button",
  Position = {110,48},
  Size = {56,56},
  Color = {0,0,0,0},
  Margin = 2,
  CornerRadius = 2,
  StrokeWidth = 0
}

layout["bridge.video.peripheral.active.icon"] = {
  PrettyName = "USB Video Active",
  Style = "Button",
  Position = {170,48},
  Size = {56,56},
  Color = {0,0,0,0},
  Margin = 2,
  CornerRadius = 2,
  StrokeWidth = 0
}

layout["bridge.audio.peripheral.active.icon"] = {
  PrettyName = "USB Audio Active",
  Style = "Button",
  Position = {230,48},
  Size = {56,56},
  Color = {0,0,0,0},
  Margin = 2,
  CornerRadius = 2,
  StrokeWidth = 0
}

table.insert(graphics,{
  Type = "Label", Text = "USB Video Stats",
  HTextAlign = "Left",
  VTextAlign = "Left",
  Position = {8,112}, Size = {90,16},
})

layout["bridge.video.peripheral.combined.stats"] = {
  PrettyName = "USB Video Stats",
  Style = "Indicator",
  TextBoxStyle = "Normal",
  Position = {98,112},
  HTextAlign = "Left",
  Size = {220,16},
  Padding = 3
}

table.insert(graphics,{
  Type = "Label", Text = "USB Audio Stats",
  HTextAlign = "Left",
  VTextAlign = "Center",
  Position = {8,146}, Size = {90,16},
})

layout["bridge.audio.peripheral.combined.stats"] = {
  PrettyName = "USB Audio Stats",
  Style = "Indicator",
  TextBoxStyle = "Normal",
  Position = {98,146},
  HTextAlign = "Left",
  Size = {220,16},
  Padding = 3
}