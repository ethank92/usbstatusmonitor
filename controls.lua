table.insert(ctrls,{
  Name = "named.status.component", 
  ControlType = "Text", TextBoxType = "ComboBox",
  PinStyle = "Input", UserPin = true, 
  Count = 1
})

custom_icon = "--[[ #encode "images/usb_disconnected.png" ]]"

table.insert(ctrls, {
  Name = "bridge.video.peripheral.usb.speed.icon",
  ControlType = "Indicator",
  IndicatorType = "LED",
  Count = 1,
  PinStyle = "Output",
  UserPin = true,
  IconType = "Image",
  Icon = custom_icon
})

custom_icon = "--[[ #encode "images/video_inactive.png" ]]"

table.insert(ctrls, {
  Name = "bridge.video.peripheral.active.icon",
  ControlType = "Indicator",
  IndicatorType = "LED",
  Count = 1,
  PinStyle = "Output",
  UserPin = true,
  IconType = "Image",
  Icon = custom_icon
})

custom_icon = "--[[ #encode "images/audio_inactive.png" ]]"

table.insert(ctrls, {
  Name = "bridge.audio.peripheral.active.icon",
  ControlType = "Indicator",
  IndicatorType = "LED",
  Count = 1,
  PinStyle = "Output",
  UserPin = true,
  IconType = "Image",
  Icon = custom_icon
})

table.insert(ctrls, {
  Name = "bridge.video.peripheral.active.usb",
  ControlType = "Indicator",
  IndicatorType = "LED",
  Count = 1,
  PinStyle = "Output",
  UserPin = true,
})

table.insert(ctrls, {
  Name = "bridge.video.peripheral.combined.stats",
  ControlType = "Indicator",
  IndicatorType = "Text",
  Count = 1,
  PinStyle = "Output",
  UserPin = true
})

table.insert(ctrls, {
  Name = "bridge.audio.peripheral.combined.stats",
  ControlType = "Indicator",
  IndicatorType = "Text",
  Count = 1,
  PinStyle = "Output",
  UserPin = true
})

table.insert(ctrls, {
  Name = "bridge.video.peripheral.stream1.stats.instant.fps",
  ControlType = "Indicator",
  IndicatorType = "Text",
  Count = 1,
  PinStyle = "Output",
  UserPin = true
})

table.insert(ctrls, {
  Name = "bridge.video.peripheral.stream1.frame.count.number",
  ControlType = "Indicator",
  IndicatorType = "Text",
  Count = 1,
  PinStyle = "Output",
  UserPin = true
})

table.insert(ctrls, {
  Name = "bridge.video.peripheral.stats.usb.avg.mbps",
  ControlType = "Indicator",
  IndicatorType = "Text",
  Count = 1,
  PinStyle = "Output",
  UserPin = true
})

table.insert(ctrls, {
  Name = "bridge.video.peripheral.usb.video.format",
  ControlType = "Indicator",
  IndicatorType = "Text",
  Count = 1,
  PinStyle = "Output",
  UserPin = true
})

table.insert(ctrls, {
  Name = "bridge.video.peripheral.stats.usb.encoding",
  ControlType = "Indicator",
  IndicatorType = "Text",
  Count = 1,
  PinStyle = "Output",
  UserPin = true
})

table.insert(ctrls, {
  Name = "bridge.video.peripheral.stats.usb.speed",
  ControlType = "Indicator",
  IndicatorType = "Text",
  Count = 1,
  PinStyle = "Output",
  UserPin = true
})

table.insert(ctrls, {
  Name = "bridge.video.peripheral.usb.src.minus",
  ControlType = "Indicator",
  IndicatorType = "Text",
  Count = 1,
  PinStyle = "Output",
  UserPin = true
})

table.insert(ctrls, {
  Name = "usb.speed",
  ControlType = "Indicator",
  IndicatorType = "Text",
  Count = 1,
  PinStyle = "Output",
  UserPin = true
})